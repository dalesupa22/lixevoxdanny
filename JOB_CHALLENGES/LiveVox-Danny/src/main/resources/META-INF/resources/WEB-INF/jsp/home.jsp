<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title>PhoneBook</title>
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	<div class="container">
		<div class="pure-g">
			<div class="pure-u-1">
				<div class="header">
					<img class="logo" src="img/phonebook.png"/>
					<p>v 1.0</p>
				</div>
				
			</div>
		</div>
		<div class="pure-g">
		    <div class="pure-u-sm-1 pure-u-1-3">
		    	<div class="box">
		    		
		    		<c:choose>
					    <c:when test="${empty edit}">
					        <h2><i class="fa fa-user-plus"></i>New contact</h2>
					        <form action='/home/add' method='post' class="pure-form">
							    <fieldset class="pure-group">
							        <input type="text"  name='firstName'  class="pure-input-1-2"  value="${user.firstName}" placeholder="First Name">
							        <input type="text"  name='lastName'  class="pure-input-1-2" value="${user.lastName}" placeholder="Last Name">
							        <input type="text"  name='phone'  class="pure-input-1-2" value="${user.phone}" placeholder="Phone">
							    </fieldset>
							    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
							    <i class="fa fa-user-plus"></i>Add</button>
							</form>
					    </c:when>
					    <c:otherwise>
					        <h2><i class="fa fa-user-plus"></i>Contact Edition</h2>
					        <form action='/home/update' method='post' class="pure-form">
							    <fieldset class="pure-group">
							        <input type="text"  name='firstName'  class="pure-input-1-2"  value="${user.firstName}" placeholder="First Name">
							        <input type="text"  name='lastName'  class="pure-input-1-2" value="${user.lastName}" placeholder="Last Name">
							        <input type="text"  name='phone'  class="pure-input-1-2" value="${user.phone}" placeholder="Phone">
							        <input type='hidden' id='id'  class='form-control' name='id' value="${user.id}"/>
							    </fieldset>
							    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
							    <i class="fa fa-save"></i>Update</button>
							</form>
					    </c:otherwise>
					</c:choose>
		    		
		    		
				</div>
			</div>
		    <div class="pure-u-sm-1 pure-u-1-3">
				<div class="box">
		    		<h2><i class="fa fa-search"></i>Search contact</h2>
		    		<form class="pure-form" method='post' action='/home/search/'> 
		    			<fieldset class="pure-group">
					    	<input type="text" name='parameter' class="pure-input-1-2">
					     </fieldset>
					    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
					    <i class="fa fa-search"></i>Search</button>
					</form>
				</div>
			</div>
			<div class="pure-u-sm-1 pure-u-1-3">
				<div class="box">
		    		<h2><i class="fa fa-users"></i> Contacts</h2>
		    		<c:if test="${not empty search}">
    					Query results  <a href="/home/cleanresults">
						                <button type="submit" class="btn btn-primary">Clean Search</button>
						                </a>
					</c:if>
	    			<table class="pure-table">
					    <thead>
					        <tr>
					            <th>First Name</th>
					            <th>Last Name</th>
					            <th>Phone</th>
					        </tr>
					    </thead>
					
					    <tbody>
					    
					      <c:forEach items="${list}" var="lou">
						      <tr>
						        <td>${lou.firstName}</td>
						        <td>${lou.lastName}</td>
						        <td>${lou.phone}</td>
						
									 <td>
						             <a href="/home/edit/${lou.id}">
						                
						                
						                 <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
					    <i class="fa fa-pencil"></i></button>
						                </a>
						            </td>
						            <td>
						             <a href="/home/delete/${lou.id}">
						                 <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
					    <i class="fa fa-remove"></i></button>
					    
						                </a>
						            </td>
						      </tr>
						    </c:forEach>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>