package com.app;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author Danny Suarez
 */

@Controller
@RequestMapping("/home")
public class HomeController {

	private IUserService userService;

	@Autowired
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
		
		Boolean commingFromSearch = (Boolean)request.getSession().getAttribute("search");
		Boolean edit = (Boolean)request.getSession().getAttribute("edit");
		List<User> users=new ArrayList<User>();
		
		if(commingFromSearch!=null) {
			ModelAndView model = new ModelAndView("home");
			users=(List<User>)request.getSession().getAttribute("results");
			model.addObject("list", users);
			User newUser=new User();
			model.addObject("user", newUser);
			return model;
			
		}
		if(edit!=null) {
			ModelAndView model = new ModelAndView("home");
			User editUser=(User)request.getSession().getAttribute("user");
			model.addObject("user", editUser);
			model.addObject("list", userService.listAllUsers());
			return model;
		}
		
		else {
			ModelAndView model = new ModelAndView("home");
			model.addObject("list", userService.listAllUsers());
			User newUser=new User();
			model.addObject("user", newUser);
			return model;
		}

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteUsers(HttpServletRequest request,@PathVariable String id) {
		helperCleanSessionParameters(request);
		userService.deleteUser(id);
		return new ModelAndView("redirect:/home");
	}

	
	@RequestMapping(value = "/cleanresults", method = RequestMethod.GET)
	public ModelAndView deleteUsers(HttpServletRequest request) {
		helperCleanSessionParameters(request);
		return new ModelAndView("redirect:/home");
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView searchUsers(HttpServletRequest request,@RequestParam String parameter) {
		List<User> users=userService.findUserByInput(parameter);
		request.getSession().setAttribute("search",true);
		request.getSession().setAttribute("results",users);
		return new ModelAndView("redirect:/home");
	}
	
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ModelAndView userRegister(HttpServletRequest request,@ModelAttribute("user")User user){
		helperCleanSessionParameters(request);
		ModelAndView model = new ModelAndView("home");
		if(user!=null){
			userService.saveUser(user);
			model.addObject("warning", "User Registration Success");

		}
		else{
			model.addObject("danger","Something Going Bad" );

		}
		return new ModelAndView("redirect:/home");
	}
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(HttpServletRequest request,@PathVariable("id") String id) {
		helperCleanSessionParameters(request);
		request.getSession().setAttribute("edit",true);
		User user = userService.getUserById(id);
//		ModelAndView model = new ModelAndView("edit");
//		model.addObject("user", user);
		request.getSession().setAttribute("user",user);
		return new ModelAndView("redirect:/home");
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request,@ModelAttribute("user")User user) {
		helperCleanSessionParameters(request);
//		User user = userService.getUserById(id);
		user.setFirstName(user.getFirstName());
		user.setLastName(user.getLastName());
		user.setPhone(user.getPhone());
		user.setId(user.getId());
		userService.saveUser(user);
		
		request.getSession().removeAttribute("edit");
		request.getSession().removeAttribute("user");
		return new ModelAndView("redirect:/home");
	}
	
	public void helperCleanSessionParameters(HttpServletRequest request) {
		request.getSession().removeAttribute("search");
		request.getSession().removeAttribute("results");
	}

}
