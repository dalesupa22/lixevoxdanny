/**
 * 
 */
package com.app;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Danny Suarez
 *
 */

@Service
public class UserServiceImpl implements IUserService{

	private IUser userRepo;
	
	@Autowired
	public void setUserRepo(IUser userRepo) {
		this.userRepo = userRepo;
	}

	@Override
	public Iterable<User> listAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public User getUserById(String id) {
		Optional<User> users=userRepo.findById(id);
		return users.get();
	}

	@Override
	public User saveUser(User user) {
		return userRepo.save(user);
	}
	
	@Override
	public void deleteUser(String id) {
		 userRepo.deleteById(id);
	}
	
	@Override
	public List<User> findUserByInput(String parameter) {
		 return userRepo.findUsersWithPartOfName(parameter);
	}
	
	
	
}
