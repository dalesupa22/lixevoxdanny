/**
 * 
 */
package com.app;

import java.util.List;

/**
 * @author Danny Suarez
 */


public interface IUserService {

	   public Iterable<User> listAllUsers();

	   public User getUserById(String id);

	   public User saveUser(User user);
	    
	   public void deleteUser(String id);
	   
	   public List<User> findUserByInput(String parameter);
	
}
