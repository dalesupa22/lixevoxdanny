package com.app;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author Danny Suarez
 */

@Controller
@RequestMapping("/")
public class WelcomeController {


	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
			ModelAndView model = new ModelAndView("welcome");
			return model;
		}


}
