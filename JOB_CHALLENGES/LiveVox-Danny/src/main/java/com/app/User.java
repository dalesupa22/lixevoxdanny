/**
 * 
 */
package com.app;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Danny Suarez
 */

@Entity
@Table(name = "USER")					
public class User {				

	@Id
	@Column(name="ID")
	private String id=UUID.randomUUID().toString();
	@NotNull
	@Column(name="FIRST_NAME")
	private String firstName;
	@Column(name="LAST_NAME")
	private String lastName;
	@Column(name="PHONE")
	private String phone;
	private Date creationDate;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public User() {
		super();
	}
	public User( String firstName, String lastName, String phone,Date creationDate) {
		super();
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.creationDate = creationDate;
	}
	
	
}
