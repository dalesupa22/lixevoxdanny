/**
 * 
 */
package com.app;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author Danny Suarez
 */

public interface IUser extends CrudRepository<User, String> {
	
	@Query("SELECT u FROM User u WHERE (u.firstName LIKE CONCAT('%',:firstName,'%'))"
			+ " or (u.lastName LIKE CONCAT('%',:firstName,'%'))"
			+ " or (u.phone LIKE CONCAT('%',:firstName,'%'))")
	public List<User> findUsersWithPartOfName(@Param("firstName") String firstName);

}
